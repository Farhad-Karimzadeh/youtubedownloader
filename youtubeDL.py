import tkinter as tk
from tkinter.filedialog import askdirectory
from pytube import YouTube
from tkinter import messagebox

window = tk.Tk()
window.title("Youtube Downloader")
window.minsize(360,130)
window.maxsize(360,130)
window.configure(bg = "#ABEBC6")
window.iconbitmap("yt.ico")

def wedget():
    lnk_lbl = tk.Label(window ,text="Video link:")
    lnk_lbl.grid(row=0,column=0,padx=(10,5),pady=(20,5))
    lnk_lbl.config(font=("None",12),fg= '#1E8449',bg="#ABEBC6")

    lnk_input = tk.Entry(window, width=40,textvariable=video_lnk)
    lnk_input.grid(row=0,column=1,padx= (0,5),pady=(20,5))

    place_lbl = tk.Label(window ,text="Save to:")
    place_lbl.grid(row=1,column=0)
    place_lbl.config(font=("None",12),fg= '#1E8449',bg="#ABEBC6")

    place_input = tk.Entry(window, width=29,textvariable=download_dir )
    place_input.grid(row=1,column=1,sticky="w")

    place_btn = tk.Button(window,text="browse",width=7, height=1,command=browse)
    place_btn.grid(row=1,column=1,sticky="e",padx=(0,5))
    place_btn.config(bg="#196F3D",fg="#D5F5E3")

    download_btn = tk.Button(window,text="D  O  W  N  L  O  A  D",width=33,height=1,command=download)
    download_btn.grid(row=2,column=1,sticky="w",padx=1,pady=5)
    download_btn.config(bg="#196F3D",fg="#D5F5E3")

def browse():
    directory_input = askdirectory(initialdir="YOUR DIRECTORY PATH",title='save')
    download_dir.set(directory_input)
        
download_dir = tk.StringVar()   
video_lnk = tk.StringVar()   

def download():
    link = video_lnk.get()
    save_dir = download_dir.get()
    yt = YouTube(link)
    yt.streams.first().download(save_dir)
    messagebox.showinfo(title="Success...",message="Your video downloaded...\nPowered by : Farhad Karimzadeh")
 
wedget()

window.mainloop()